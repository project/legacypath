<?php

/**
 * @file
 * The form logic for the Legacy Path module.
 */


/**
 * Build the Legacy Path settings form.
 *
 * @return
 *  The form array.
 */
function legacypath_settings_form() {
  $form = array();

  $form['legacypath'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Legacy Path Settings'),
    '#weight' => -2,
  );

  $times   = array(86400, 604800, 2419200, 7257600, 15724800, 31536000);
  $options = drupal_map_assoc($times, 'format_interval');
  $options = array(0 => t('Do Not Cache')) + $options;

  $form['legacypath']['legacypath_cache_lifespan'] = array(
    '#type'          => 'select',
    '#title'         => t('Cache Lifespan'),
    '#options'       => $options,
    '#default_value' => variable_get('legacypath_cache_lifespan', 3600),
  );

  $description = t('The normal behavior of Legacy Path is to only perform a redirect if an incoming URL does not match a Drupal path. Checking this box will override that behavior and cause redirects anytime a redirect expression is matched, even if the incoming URL matches a Drupal path.');
  $form['legacypath']['legacypath_always_redirect'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Force Redirect If Exists'),
    '#default_value' => variable_get('legacypath_always_redirect', FALSE),
    '#description'   => $description,
  );

  $form['redirects'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('Path Redirect Rules'),
    '#weight' => 2,
  );

  $result = db_query('SELECT * from {legacypath_redirects} ORDER BY weight ASC');
  $titles = array();
  while ($row = db_fetch_array($result)) {
    $id = $row['rid'];
    
    $form[$id]['id'] = array(
      '#type'  => 'value',
      '#value' => $id,
    );
    
    $form[$id]['name']   = array('#value' => $row['name']);
    $form[$id]['weight'] = array('#value' => $row['weight']);
    $form[$id]['status'] = array('#value' => $row['status']);
    $form[$id]['edit']   = array('#value' => l(t('Edit'), LEGACYPATH_BASE . '/edit/'. $id));
    $form[$id]['delete'] = array('#value' => $default ? '' : l(t('Delete'), LEGACYPATH_BASE . '/delete/'. $id));
  }

  return system_settings_form($form);
}


/**
 * The form for creating and editing new redirect rules.
 *
 * @param $rid
 *  If editing an existing redirect rule, the unique ID of that rule.
 *
 * @return
 *  The form elements array.
 */
function legacypath_create_form($rid = 0) {
  $form = array();
  
  if ($rid > 0) {
    $result   = db_query('SELECT * FROM {legacypath_redirects} WHERE rid = %d', $rid);
    $existing = db_fetch_object($result);
  }
  else {
    $existing = NULL;
  }
  
  $form['legacypath'] = array(
    '#type'   => 'fieldset',
    '#title'  => ($existing !== NULL) ? t('Edit Existing Redirect')
                                      : t('Create New Redirect'),
    '#weight' => -2,
  );
  
  $form['legacypath']['redirect_id'] = array(
    '#type'  => 'value',
    '#value' => ($existing !== NULL) ? $existing->rid : -1,
  );
  
  $form['legacypath']['name'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Name'),
    '#default_value' => ($existing !== NULL) ? $existing->name : '',
    '#description'   => t("A unique name to help you remember what this redirect rule does when it's shown in the list view."),
    '#required'      => TRUE,
  );  
  
  $default     = _legacypath_default_local_uri();
  $description = t('A %pcre for matching the contents of the incoming URL. The default expression %default will match all incoming URIs. This expression must begin and end with a forward slash ( / ) character.',
                   array('%pcre'    => t('Perl-Compatible Regular Expression'),
                         '%default' => $default));
  $form['legacypath']['source'] = array(
    '#type'          => 'textfield',
    '#title'         => 'Source Path Expression',
    '#default_value' => ($existing != NULL) ? $existing->source_expr : $default,
    '#description'   => $description,
    '#maxlength'     => 512,
    '#required'      => TRUE,
  );  
  
  $description = t('A substitution for the source expression above. This expression may contain references to text captured in parenthesis in the source path expression using the %expr syntax (where n is the number of the capture above, beginning with 1).',
                   array('%expr' => '$n'));
  $default = 'http://example.com$1';
  $form['legacypath']['destination'] = array(
    '#type'          => 'textfield',
    '#title'         => 'Destination Path',
    '#default_value' => ($existing != NULL) ? $existing->dest_expr : $default,
    '#description'   => $description,
    '#maxlength'     => 512,
    '#required'      => TRUE,
  );
  
  $form['legacypath']['weight'] = array(
    '#type'          => 'select',
    '#title'         => t('Weight'),
    '#default_value' => ($existing != NULL) ? $existing->weight : 0,
    '#options'       => drupal_map_assoc(range(-10, 10)),
    '#description'   => t('This determines the order that rules are evaluated. Rules with lower weight values are evaluated before rules with higher weights.'),
  );
  
  $options = array(301 => t('301 Moved Permanently'), 
                   307 => t('307 Temporary Redirect'),
                   404 => t('404 Not Found'));
  $form['legacypath']['status'] = array(
    '#type'          => 'select',
    '#title'         => t('Status Code'),
    '#default_value' => ($existing != NULL) ? intval($existing->status) : 301,
    '#options'       => $options,
    '#description'   => t('When redirecting using this rule, should the redirect be permanent, temprorary, or not found?'),
  );
  
  $form['submit'] = array(
    '#type'  => 'submit', 
    '#value' => t('Save Redirect Rule'),
  );
  
  $form['cancel'] = array(
    '#type'  => 'submit',
    '#value' => t('Cancel'),
  );
      
  return $form;
}


/**
 * Validate the redirect editing/creation form.
 *
 * @param $form_id
 *  The ID of the form.
 * @param $form_values
 *  The form values.
 */
function legacypath_create_form_validate($form_id, $form_values) {

  // The source pattern must begin and end with a slash (/)/
  $source = trim($form_values['source']);
  if (($source[0] != '/') || ($source[strlen($source) - 1] != '/')) {
    $msg = t('The source expression must begin and end with a slash ( / ) character.');
    form_set_error('source', $msg);
  }
}


/**
 * Handle the redirect editing/creation form submit.
 *
 * @param $form_id
 *  The ID of the form.
 * @param $form_values
 *  The form values.
 *
 * @return
 *  The Drupal path to display after the submit process is complete.
 */
function legacypath_create_form_submit($form_id, $form_values) {
  $rid = $form_values['redirect_id'];
  
  if ($rid == -1) {
    $is_new = TRUE;
    $rid    = db_next_id('{legacypath_redirects}_rid');
  }
  else {
    $is_new = FALSE;
  }
  
  $name   = $form_values['name'];
  $source = $form_values['source'];
  $dest   = $form_values['destination'];
  $weight = $form_values['weight'];
  $status = $form_values['status'];
  
  if ($is_new) {
    $query = 'INSERT INTO {legacypath_redirects} ' .
             '            (name, weight, source_expr, dest_expr, status, rid) ' .
             "       VALUES ('%s', %d, '%s', '%s', %d, %d)";
  }
  else {
    $query = 'UPDATE {legacypath_redirects} SET ' .
                     "name = '%s', weight = %d, source_expr = '%s'," .
                     "dest_expr = '%s', status = %d " .
                     'WHERE rid = %d';
  }
  
  $result = db_query($query, $name, $weight, $source, $dest, $status, $rid);
  
  if ($result !== FALSE) {
    $op   = $is_new ? t('Created') : t('Updated');
    $subs = array('@op' => $op, '@name' => $name);
    drupal_set_message(t('@op redirect @name', $subs));
  }
  else {
    $op   = $is_new ? t('create') : t('update');
    $subs = array('@op' => $op, '@name' => $name);
    drupal_set_message(t('Could not @op redirect @name', $subs), 'error');
  }
  
  return LEGACYPATH_BASE;
}


/**
 * Build the default local URI expression for this Drupal installation.
 *
 * @return
 *  A regular expression matching the local installation.
 */
function _legacypath_default_local_uri() {
  $proto   = $_SERVER['HTTPS'] ? 'https:\/\/' : 'http:\/\/';
  $host    = $_SERVER['SERVER_NAME'];
  $port    = ($_SERVER['SERVER_PORT'] == 80 ? '' : ':'. $_SERVER['SERVER_PORT']);
  $uri     = $proto . $host . $port;
  $default = '/' . $uri . '(\/.*)/';
  
  return $default;
}


/**
 * Generate the confirmation form for deleting a redirect.
 *
 * @param $id
 *  The ID of the redirect to delete.
 *
 * @return
 *  The form elements.
 */
function legacypath_delete_form($id) {
  $form = array();
  
  $result = db_query('SELECT * FROM {legacypath_redirects} WHERE rid = %d', $id);
  $existing = db_fetch_object($result);
  
  if (!isset($existing->rid)) {
    drupal_set_message(t('Could not find redirect with ID @id.', 
                         array('@id' => $id)), 'error');
    return;
  }
  
  $form['confirm'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Confirm Delete'),
    '#description' => t('You are about to remove the redirect below. This action cannot be undone.'),
    '#collapsible' => FALSE,
  );
 
  $form['confirm']['redirect']['name']['left'] = array(
    '#type'  => 'item',
    '#title' => t('Name'),
  );
  
  $form['confirm']['redirect']['name']['right'] = array(
    '#prefix' => '<div>',
    '#value'  => $existing->name,
    '#suffix' => '</div>',
  );
  
  $form['confirm']['redirect']['source']['left'] = array(
    '#type' => 'item',
    '#title'=> t('Source Expression'),
  );
  
  $form['confirm']['redirect']['source']['right'] = array(
    '#prefix' => '<div>',
    '#value'  => $existing->source_expr,
    '#suffix' => '</div>',
  );
  
  $form['confirm']['redirect']['dest']['left'] = array(
    '#type' => 'item',
    '#title' => t('Destination'),
  );
  
  $form['confirm']['redirect']['dest']['right'] = array(
    '#prefix' => '<div>',
    '#value'  => $existing->dest_expr,
    '#suffix' => '</div>',
  );
  
  $form['redirect_id'] = array(
    '#type'  => 'value',
    '#value' => $id,
  );
  
  $form['redirect_name'] = array(
    '#type'  => 'value',
    '#value' => $existing->name,
  );
  
  $form['submit'] = array(
    '#type'  => 'submit', 
    '#value' => t('Delete'),
  );
  
  $form['cancel'] = array(
    '#type'  => 'submit', 
    '#value' => t('Cancel'),
  );
  
  return $form;
}


/**
 * Handle the delete form submission.
 *
 * @param $form_id
 *  The form's ID
 * @param $form_values
 *  The form values.
 *
 * @return
 *  The path to send the browser to after the form is submitted.
 */
function legacypath_delete_form_submit($form_id, $form_values) {
  if ($form_values['op'] == t('Delete')) {
    $id = $form_values['redirect_id'];
    
    $result = db_query('DELETE FROM {legacypath_redirects} WHERE rid = %d', $id);
    $vars = array('@id' => $id, '@name' => $form_values['redirect_name']);
    
    if ($result !== FALSE) {
      drupal_set_message(t("Deleted redirect '@name' (ID @id).", $vars));
    }
    else {
      drupal_set_message(t("Could not delete redirect '@name' (ID @id).", $vars));
    }
  }
  
  return LEGACYPATH_BASE;
}


/**
 * Theme the tabular layout of the redirect being deleted.
 *
 * @param $form
 *  The delete confirmation form.
 *
 * @return
 *  The themed form.
 */
function theme_legacypath_delete_form($form) {
  $rows = array();
  
  $base = $form['confirm']['redirect'];
  
  foreach ($base as $name => $element) {
    if (isset($element['left']) && is_array($element['left'])) {
      $rows[] = array(
        array(
          'data'  => drupal_render($element['left']), 
          'class' => 'legacypath-del-left'
        ),
        array(
          'data' => drupal_render($element['right']),
          'class' => 'legacypath-del-right'
        ),
      );
      
      unset($form['confirm']['redirect'][$name]);
    }
  }

  $form['confirm']['redirect'] = array(
    '#value' => theme('table', array(), $rows, array('class' => 'title-rewrite-delete-table')),
  );
  
  return drupal_render($form);
}


/**
 * Theme the table in the admin settings.
 *
 * @param $form
 *  The form elements to theme.
 *
 * @return
 *  The rendered form elements.
 */
function theme_legacypath_settings_form($form) {
  $rows = array();
  
  foreach ($form as $name => $element) {
    if (isset($element['name']) && is_array($element['name'])) {
      $rows[] = array(
        drupal_render($element['name']),
        drupal_render($element['weight']),
        drupal_render($element['status']),
        drupal_render($element['edit']),
        drupal_render($element['delete'])
      );
      unset($form[$name]);
    }
  }
  
  $header = array(t('Name'), t('Weight'), t('HTTP Status'), t('Edit'), t('Delete'));
  $form['redirects']['table'] = array(
    '#value' => theme('table', $header, $rows),
  );

  return drupal_render($form);
}
