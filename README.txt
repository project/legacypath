
LEGACY PATH MODULE - README
______________________________________________________________________________

NAME:       Legacy Path
AUTHORS:    Shannon Lucas <shannon@fourkitchens.com>
______________________________________________________________________________

DESCRIPTION

Legacy Path is intended to support websites that are moving to Drupal but
have content that either won't be managed by Drupal immediately or at all,
but must remain reachable through it's original URL. The assumption is
made that the legacy system remains operational, but at a different
(sub)domain or directory.


INSTALLATION

Step 1)
  Place the entire legacypath directory into a modules directory of your
  choice, for example: sites/all/modules.

Step 2)
  Enable the Legacy Path module by navigating to
    Administer > Site building > Modules

Step 3)
  Go to "Administer > Site Configuration > Legacy Path Redirect" to 
  configure the module.

Step 4)
  Go to "Administer > Site Configuration > Legacy Path Redirect > 
  New Path Redirect" to add a redirect rule.


CREDITS & SUPPORT

All issues with this module should be reported via the following form:
http://drupal.org/node/add/project_issue/legacypath

______________________________________________________________________________
http://fourkitchens.com/
